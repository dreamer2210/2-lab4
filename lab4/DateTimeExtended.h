#pragma once 
#include "datetime.h"

class DateTimeExtended : public DateTime
{
private:
	unsigned int milsec;

	int GetSec();
public:
	DateTimeExtended(unsigned int milsec);
};