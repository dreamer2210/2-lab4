#pragma once
#include <time.h>
#include <ostream>
#include <istream>
#include <string>
#include <fstream>

class DateTime
{
protected:
    int hour;
    int minute;
    int second;

    int day;
    int month;
    int year;

public:
    DateTime() {}
    ~DateTime() {}

    time_t Get_unix_time(DateTime A);
    void System_time(DateTime& system);
    void Get_next_date(DateTime& next);
    void Get_prev_date(DateTime& prev);
    void Get_normal_time(time_t unix_time, DateTime& A);

    friend std::ostream& operator << (std::ostream& stream, const DateTime& dt);
    friend std::istream& operator >> (std::istream& stream, DateTime& dt);

    int& Get_minute();
    int& Get_hour();
    int& Get_second();
    int& Get_day();
    int& Get_month();
    int& Get_year();

	virtual int GetSec();
};