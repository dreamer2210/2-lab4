#pragma once 
#include "datetime.h"

class DateTimeExtended : public DateTime
{
private:
	unsigned int milsec;
public:
	int GetSec();

	DateTimeExtended(unsigned int milsec);
};